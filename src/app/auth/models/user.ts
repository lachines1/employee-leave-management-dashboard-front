﻿import {Role} from './role';
import {Team} from "../../shared/models/team.model";
import {OrganizationalUnit} from "../../shared/models/organizational-unit.model";

export class User {
    id?: number;
    email?: string;
    password?: string;
    firstName?: string;
    phone?: string;
    gender?: string;
    leaveDays?: number;
    onLeave?: boolean;
    externalActivitiesLimit?: number;
    lastName?: string;
    avatar?: string;
    team?: Team;
    organizationalUnit?: OrganizationalUnit;
    role?: Role;
    token?: string;

}
