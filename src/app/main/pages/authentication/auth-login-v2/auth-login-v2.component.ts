import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {takeUntil, first} from 'rxjs/operators';
import {Subject} from 'rxjs';

import {AuthenticationService} from 'app/auth/service';
import {CoreConfigService} from '@core/services/config.service';
import {AuthenticationRequest} from "../../../../shared/models/authentication-request.model";
import {AuthenticationResponse} from "../../../../shared/models/authentication-response.model";
import {JwtUtilService} from "../../../../shared/services/jwt.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {NgxUiLoaderService} from "ngx-ui-loader";
import {VerificationRequest} from "../../../../shared/models/verification-request.model";

@Component({
    selector: 'app-auth-login-v2',
    templateUrl: './auth-login-v2.component.html',
    styleUrls: ['./auth-login-v2.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AuthLoginV2Component implements OnInit {
    //  Public
    public coreConfig: any;
    public loginForm: FormGroup;
    public loading = false;
    public submitted = false;
    public returnUrl: string;
    public error = '';
    @ViewChild('content') content: any;
    public passwordTextType: boolean;
    authRequest: AuthenticationRequest = {};
    otpCode = '';
    authResponse: AuthenticationResponse = {};

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CoreConfigService} _coreConfigService
     * @param _formBuilder
     * @param _route
     * @param _router
     * @param _authenticationService
     * @param jwtService
     * @param ngxLoader
     * @param authService
     * @param router
     * @param modalService
     */
    constructor(
        private _coreConfigService: CoreConfigService,
        private _formBuilder: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private _authenticationService: AuthenticationService,
        private jwtService: JwtUtilService,
        private ngxLoader:NgxUiLoaderService,
        private authService: AuthenticationService, private router: Router,private modalService:NgbModal
    ) {
        // redirect to home if already logged in
        if (this._authenticationService.currentUserValue) {
            this._router.navigate(['/apps/dashboard']);
        }

        this._unsubscribeAll = new Subject();

        // Configure the layout
        this._coreConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                menu: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                customizer: false,
                enableLocalStorage: false
            }
        };
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls;
    }

    /**
     * Toggle password
     */


    onSubmit() {
        this.submitted = true;
        this.authenticate();
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        // Login
        this.loading = true;
        this._authenticationService
            .login(this.authRequest)
            .pipe(first())
            .subscribe(
                data => {
                    this._router.navigate([this.returnUrl]);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                }
            );
    }

    // Lifecycle Hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            email: ['admin@demo.com', [Validators.required, Validators.email]],
            password: ['admin', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = '/apps/dashboard';

        // Subscribe to config changes
        this._coreConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe(config => {
            this.coreConfig = config;
        });
    }
    authenticate() {
        this.ngxLoader.start()
        this._authenticationService.login(this.authRequest)
            .subscribe({
                next: (response) => {
                    this.authResponse = response;
                    console.log(response);
                    if (!this.authResponse.mfaEnabled) {
                        localStorage.setItem('token', response.accessToken as string);
                        this.jwtService.setToken(response.accessToken as string);
                        localStorage.setItem('currentUser', JSON.stringify(this.jwtService.getUser()));
                        console.log(this.jwtService.getUser());
                        this._router.navigate([this.returnUrl]).then(r => window.location.reload())
                    } else {
                        this.openSignUpModal();
                    }
                    this.ngxLoader.stop()
                },
                complete: () => {

                       this._router.navigate([this.returnUrl])

                }
            });
    }

    // Define a separate function for navigation
    private navigateToDashboard() {
        this._router.navigate(['apps/dashboard']);
    }
    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }




    close(id: string) {
        this.modalService.dismissAll(id);
    }
    openSignUpModal() {
        this.modalService.open(this.content, {
            size: 'lg',
            centered: true,
            windowClass: 'modal-warning'
        });
    }
    togglePasswordTextType() {
        this.passwordTextType = !this.passwordTextType;
    }

    verifyCode() {
        const verifyRequest: VerificationRequest = {
            email: this.authRequest.email,
            code: this.otpCode
        };
        this._authenticationService.verifyCode(verifyRequest)
            .subscribe({
                next: (response) => {
                    localStorage.setItem('token', response.accessToken);
                    this.jwtService.setToken(response.accessToken);
                    localStorage.setItem('currentUser', JSON.stringify(this.jwtService.getUser()));
                    console.log(this.jwtService.getUser());
                    this._router.navigate([this.returnUrl]).then(r => window.location.reload())
                }
            });
    }

    //protected readonly close = close;
}
