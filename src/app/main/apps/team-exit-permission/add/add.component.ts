import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserListService} from "../../user/user-list/user-list.service";
import {ToastrService} from "ngx-toastr";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Router} from "@angular/router";
import {TeamExitPermissionService} from "../../../../shared/services/team-exit-permission.service";
import {TeamExitPermissionRequest} from "../../../../shared/models/team-exit-permission-request.model";
import {TeamService} from "../../../../shared/services/team.service";

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AddComponent implements OnInit {

    public durations = [
        {name: '30Mn', value: 'ThirtyMinutes'},
        {name: '1H', value: 'OneHour'},
        {name: '1H30', value: 'NinetyMinutes'},
        {name: '2H', value: 'TwoHours'},
    ]
    contentHeader: object;
    users: any[];
    reason: any;
    allTeams: any;
    teamName: string;
    protected date: Date;
    protected leaveDuration: any;
    private currentUser: any;

    constructor(private teamExitPermissionService: TeamExitPermissionService,
                private teamService: TeamService,
                private userService: UserListService,
                private toastr: ToastrService,
                private jwtService: JwtHelperService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.getTeams();
        const token = localStorage.getItem('token');
        this.currentUser = this.jwtService.decodeToken(token);
        this.contentHeader = {
            headerTitle: 'Create Team Exit Permission Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Team Exit Permissions Requests',
                        isLink: true,
                        link: '/apps/team-exit-permission/list'
                    },
                    {
                        name: 'Create Team Exit Permission Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };

    }

    addTeamExitPermission() {
        const teamExitPermissionRequest = new TeamExitPermissionRequest();
        teamExitPermissionRequest.date = this.date;
        teamExitPermissionRequest.leaveDuration = this.leaveDuration;
        teamExitPermissionRequest.reason = this.reason;
        teamExitPermissionRequest.teamName = this.teamName;
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;

        console.log(teamExitPermissionRequest);
        this.teamExitPermissionService.createTeamExitPermission(currentUserEmail, teamExitPermissionRequest).subscribe(
            (data) => {
                this.toastr.success('Team Exit Permission Request added successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
                this.router.navigate(['/apps/team-exit-permission/list']);
            },
            (error) => {
                this.toastr.error('Error adding team', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            }
        );
    }

    onDurationChange(selectedItem: any) {
        this.leaveDuration = selectedItem.value;
    }

    onTeamChange(selectedItem: any) {
        this.teamName = selectedItem.value;
        console.log('Selected Team:', this.teamName);
    }

    getTeams() {
        const role = this.jwtService.decodeToken(localStorage.getItem('token')).role;
        if (role === "ADMIN") {
            this.teamService.getAllTeams().subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        } else if (role === "MANAGER") {
            const currentUserEmail = this.jwtService.decodeToken(localStorage.getItem('token')).email;
            this.teamService.getTeamsByManager(currentUserEmail).subscribe(
                (data) => {
                    this.allTeams = data;
                    this.allTeams = data.map(team => {
                        // Add a new attribute 'value' with the same value as 'name'
                        return {...team, value: team.name};
                    });
                },
                (error) => {
                    this.toastr.error('Error fetching teams', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx-toastr',
                    });
                });
        }

    }

}
