import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalAuthorizationEditComponent } from './external-authorization-edit.component';

describe('ExternalAuthorizationEditComponent', () => {
  let component: ExternalAuthorizationEditComponent;
  let fixture: ComponentFixture<ExternalAuthorizationEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExternalAuthorizationEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExternalAuthorizationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
