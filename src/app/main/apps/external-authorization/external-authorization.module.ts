import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExternalAuthorizationAddComponent} from './external-authorization-add/external-authorization-add.component';
import {ExternalAuthorizationListComponent} from './external-authorization-list/external-authorization-list.component';
import {ExternalAuthorizationViewComponent} from './external-authorization-view/external-authorization-view.component';
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {NgSelectModule} from "@ng-select/ng-select";
import {FormsModule} from "@angular/forms";
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {RouterModule} from "@angular/router";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import { ExternalAuthorizationEditComponent } from './external-authorization-edit/external-authorization-edit.component';

const routes = [
    {
        path: 'list',
        component: ExternalAuthorizationListComponent
    },
    {
        path: 'add',
        component: ExternalAuthorizationAddComponent
    },
    {
        path: 'view/:id',
        component: ExternalAuthorizationViewComponent
    },
    {
        path: 'edit/:id',
        component: ExternalAuthorizationEditComponent
    },
]

@NgModule({
    declarations: [
        ExternalAuthorizationAddComponent,
        ExternalAuthorizationListComponent,
        ExternalAuthorizationViewComponent,
        ExternalAuthorizationEditComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CardSnippetModule,
        ContentHeaderModule,
        CoreDirectivesModule,
        CsvModule,
        FormsModule,
        NgSelectModule,
        NgxDatatableModule,
        NgbDropdownModule,
    ]
})
export class ExternalAuthorizationModule {
}
