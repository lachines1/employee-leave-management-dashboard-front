import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TeamLeaveAddComponent} from './team-leave-add.component';

describe('TeamLeaveAddComponent', () => {
    let component: TeamLeaveAddComponent;
    let fixture: ComponentFixture<TeamLeaveAddComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TeamLeaveAddComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TeamLeaveAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
