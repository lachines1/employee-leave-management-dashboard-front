import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OrganizationalUnitAddComponent} from './organizational-unit-add.component';

describe('OrganizationalUnitAddComponent', () => {
    let component: OrganizationalUnitAddComponent;
    let fixture: ComponentFixture<OrganizationalUnitAddComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [OrganizationalUnitAddComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(OrganizationalUnitAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
