import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from "@angular/router";
import {OrganizationalUnitService} from "../../../../shared/services/organizational-unit.service";
import {ToastrService} from "ngx-toastr";
import {OrganizationalUnitRequest} from "../../../../shared/models/organizational-unit-request.model";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../../../../auth/models";
import {UserListService} from "../../user/user-list/user-list.service";

@Component({
    selector: 'app-organizational-unit-add',
    templateUrl: './organizational-unit-add.component.html',
    styleUrls: ['./organizational-unit-add.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrganizationalUnitAddComponent implements OnInit {

    name: string;
    teamNames: string[];
    memberEmails: string[];
    email: string;
    contentHeader: object;
    currentUser
    role
    managers: any[];
    selectUser: User[];

    constructor(private toastr: ToastrService, private organizationalUnitService: OrganizationalUnitService, private router: Router, private jwtService: JwtHelperService, private userService: UserListService) {
    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.role = this.currentUser.role
        this.email = this.currentUser.email
        this.getUsers();
        this.contentHeader = {
            headerTitle: 'Create Department',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Departments',
                        isLink: true,
                        link: '/apps/organizational-unit/list'
                    },
                    {
                        name: 'Create Leave Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }

    addOrganizationalUnit() {
        const organizationalUnitRequest = new OrganizationalUnitRequest();
        organizationalUnitRequest.name = this.name;
        organizationalUnitRequest.teamNames = this.teamNames != null ? this.teamNames : null;
        organizationalUnitRequest.memberEmails = this.memberEmails != null ? this.memberEmails : null;
        organizationalUnitRequest.managerEmail = this.email;

        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;

        this.organizationalUnitService.createOrganizationalUnit(currentUserEmail, organizationalUnitRequest).subscribe(
            (data) => {
                this.toastr.success('Department added successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
                this.router.navigate(['/apps/organizational-unit/list']);
            },
            (error) => {
                this.toastr.error('Error adding department', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            }
        );
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.selectUser = data.filter(user => user.role === 'ADMIN');
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }


    onUserSelect(selectedItem: any) {
        this.email = selectedItem.email;
    }
}
