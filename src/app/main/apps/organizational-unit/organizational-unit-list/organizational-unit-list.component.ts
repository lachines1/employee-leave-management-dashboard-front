import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ColumnMode, DatatableComponent} from "@swimlane/ngx-datatable";
import {OrganizationalUnitService} from "../../../../shared/services/organizational-unit.service";
import {OrganizationalUnit} from "../../../../shared/models/organizational-unit.model";
import Swal from "sweetalert2";

@Component({
    selector: 'app-organizational-unit-list',
    templateUrl: './organizational-unit-list.component.html',
    styleUrls: ['./organizational-unit-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrganizationalUnitListComponent implements OnInit {
    @ViewChild(DatatableComponent) table: DatatableComponent;
    public contentHeader: object;
    basicSelectedOption: number = 10;
    public rows: any;
    data: OrganizationalUnit[];
    public ColumnMode = ColumnMode;
    private tempFilterData: any;

    constructor(private departmentService:OrganizationalUnitService) {
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Departments',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'suggestions',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Global Suggestions Lists',
                        isLink: false
                    }
                ]
            }
        };
        this.getDepartments();
    }

    filterUpdate($event: KeyboardEvent) {
        // const val = ($event.target as HTMLInputElement).value.toLowerCase();
        //
        // // filter our data
        // const temp = this.data.filter(function (d) {
        //     return (d.instanceId && d.instanceId.toLowerCase().indexOf(val) !== -1) || !val;
        // });
        //
        // // update the rows
        // this.rows = temp;
        // // Whenever the filter changes, always go back to the first page
        // this.table.offset = 0;
    }


    getDepartments() {
        this.departmentService.getAllOrganizationalUnits().subscribe((res: any) => {
            console.log(res);
            this.data = res;
            this.rows = res;
            this.tempFilterData = res;
        })
    }

    deleteDepartment(name: string) {
        Swal.fire({
            title: 'Confirm',
            text: 'Are you sure you want to delete this item? You will not be able to recover it!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger ml-1'
            }
        }).then((result) => {
            if (result.isConfirmed) {
                this.departmentService.deleteOrganizationalUnitByName(name).subscribe(
                    () => {

                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            icon: 'success',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                        this.getDepartments();
                    },
                    (error) => {
                        // Error
                        // ... your error logic
                        console.log(error);
                        Swal.fire('Error', 'An error occurred while deleting the item.', 'error');
                    }
                );
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: 'The Leave Request was not deleted.',
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                });
            }
        });
    }
}

