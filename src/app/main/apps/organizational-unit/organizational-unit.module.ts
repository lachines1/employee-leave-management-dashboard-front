import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrganizationalUnitListComponent} from './organizational-unit-list/organizational-unit-list.component';
import {OrganizationalUnitViewComponent} from './organizational-unit-view/organizational-unit-view.component';
import {OrganizationalUnitAddComponent} from './organizational-unit-add/organizational-unit-add.component';
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {RouterModule} from "@angular/router";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import {CoreCardModule} from "../../../../@core/components/core-card/core-card.module";
import { OrganizationalUnitEditComponent } from './organizational-unit-edit/organizational-unit-edit.component';

const routes = [
    {
        path: 'list',
        component: OrganizationalUnitListComponent
    },
    {
        path: 'add',
        component: OrganizationalUnitAddComponent
    },
    {
        path: 'view/:id',
        component: OrganizationalUnitViewComponent
    },
    {
        path: 'edit/:id',
        component: OrganizationalUnitEditComponent
    },
]

@NgModule({
    declarations: [
        OrganizationalUnitListComponent,
        OrganizationalUnitViewComponent,
        OrganizationalUnitAddComponent,
        OrganizationalUnitEditComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CardSnippetModule,
        ContentHeaderModule,
        CoreDirectivesModule,
        CsvModule,
        FormsModule,
        NgSelectModule,
        NgxDatatableModule,
        NgbDropdownModule,
        CoreCardModule,
    ]
})
export class OrganizationalUnitModule {
}
