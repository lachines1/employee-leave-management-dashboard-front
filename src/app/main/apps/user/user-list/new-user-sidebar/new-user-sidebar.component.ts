import {Component, OnInit} from '@angular/core';
import {CoreSidebarService} from '@core/components/core-sidebar/core-sidebar.service';
import {UserListService} from "../user-list.service";
import {ToastrService} from "ngx-toastr";
import {RegisterRequest} from "../../../../../shared/models/register-request.model";

@Component({
    selector: 'app-new-user-sidebar',
    templateUrl: './new-user-sidebar.component.html'
})
export class NewUserSidebarComponent implements OnInit {
    public registerRequest: RegisterRequest = {
        role: 'USER',
        gender: 'MALE'
    }
    roles: any[] = [
        {name: 'User', value: 'USER'},
        {name: 'Manager', value: 'MANAGER'},
        {name: 'Admin', value: 'ADMIN'}
    ];


    genders: any[] = [
        {name: 'Male', value: 'MALE'},
        {name: 'Female', value: 'FEMALE'}
    ];

    /**
     * Constructor
     *
     * @param {CoreSidebarService} _coreSidebarService
     * @param userService
     * @param _toastrService
     */
    constructor(private _coreSidebarService: CoreSidebarService, private userService: UserListService, private _toastrService: ToastrService) {
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
    }

    /**
     * Submit
     *
     * @param form
     */
    addUser() {
        this.registerRequest.mfaEnabled = false;
        this.userService.addUser(this.registerRequest).subscribe({
            next: (response) => {
                console.log(response);  // Log the entire response

                this._toastrService.success(
                    'You have successfully added a new ' +
                    this.registerRequest.role +
                    ' user to the App. ',
                    ' User "Added"' + '!',
                    { toastClass: 'toast ngx-toastr', closeButton: true, positionClass: 'toast-top-right' }
                );


                    this.toggleSidebar('new-user-sidebar');

            },
            error: (err) => {
                console.log(err.error);

                // Check if the error message is "ok" and handle it accordingly
                if (err.error === 'OK') {
                    this._toastrService.success(
                        'You have successfully added a new ' +
                        this.registerRequest.role +
                        ' user to the App. ',
                        ' User "Added"' + '!',
                        { toastClass: 'toast ngx-toastr', closeButton: true, positionClass: 'toast-top-right' }
                    );

                    this.toggleSidebar('new-user-sidebar');

                } else {
                    // Display the actual error message
                    this._toastrService.error(
                        'An Error has occurred while trying to add the new user ' + err.error,
                        'An Error has occurred' + '!',
                        { toastClass: 'toast ngx-toastr', closeButton: true, positionClass: 'toast-top-right' }
                    );
                }
            },
        });
    }

    ngOnInit(): void {
    }

    onRoleChange(selectedItem: any) {
        this.registerRequest.role = selectedItem.value;
    }

    onGenderChange(selectedItem: any) {
        this.registerRequest.gender = selectedItem.value;
    }
}
