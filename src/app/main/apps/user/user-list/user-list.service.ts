import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from "../../../../shared/models/user.model";
import {environment} from "../../../../../environments/environment";
import {RegisterRequest} from "../../../../shared/models/register-request.model";
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
    providedIn: 'root'
})
export class UserListService {
    public rows: any;
    public onUserListChanged: BehaviorSubject<any>;


    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param jwtService
     */
    constructor(private _httpClient: HttpClient, private jwtService: JwtHelperService) {
        // Set the defaults
        this.onUserListChanged = new BehaviorSubject({});
    }

    getAllUsers(): Observable<User[]> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this._httpClient.get<any[]>(`${environment.apiUrl}/api/v1/users/admin/all`, httpOptions);
    }

    addUser(addRequest: RegisterRequest): Observable<void> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this._httpClient.post<void>(`${environment.apiUrl}/api/v1/users/admin/addUser`, addRequest, httpOptions);
    }

    deleteUser(email: string) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        return this._httpClient.delete(`${environment.apiUrl}/api/v1/users/admin/${email}`, httpOptions);
    }

    getUserByEmail(email: string): Observable<User> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        return this._httpClient.get<User>(`${environment.apiUrl}/api/v1/users/${email}`, httpOptions);
    }

    updateUser(email: string, user: User): Observable<User> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        const params = {
            'currentUserEmail': this.jwtService.decodeToken(token).email,
            'targetUserEmail': email
        }
        return this._httpClient.put<User>(`${environment.apiUrl}/api/v1/users/update`, user, {
            headers: httpOptions.headers,
            params: params
        });
    }

    getUserById(id: number): Observable<User> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        return this._httpClient.get<User>(`${environment.apiUrl}/api/v1/users/id/${id}`, httpOptions);
    }


    resetPassword(oldPassword: any, newPassword: any) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }
        const params = {
            'email': this.jwtService.decodeToken(token).email,
            'oldPassword': oldPassword,
            'newPassword': newPassword
        }
        return this._httpClient.post(`${environment.apiUrl}/api/v1/users/reset`, null, {
            headers: httpOptions.headers,
            params: params
        });

    }

    resetForgottenPassword(resetToken: string, newPassword: string) {
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${resetToken}`
            }
        }
        const params = {
            'token': resetToken,
            'newPassword': newPassword
        }

        return this._httpClient.post(`${environment.apiUrl}/api/v1/users/reset-forgotten-password`, null, {
            headers: httpOptions.headers,
            params: params
        });
    }

    sendResetPasswordEmail(email: string) {
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const params = {
            'email': email
        }
        return this._httpClient.post(`${environment.apiUrl}/api/v1/users/forgot-password`, null, {
            headers: httpOptions.headers,
            params: params
        });

    }

    getUsersByManager(currentUserEmail: any) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        }

        const params = {
            'email': currentUserEmail
        }

        return this._httpClient.get<any[]>(`${environment.apiUrl}/api/v1/users/manager/get-users`, {
            headers: httpOptions.headers,
            params: params
        });
    }
}
