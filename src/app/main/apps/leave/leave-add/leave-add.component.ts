import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {LeaveService} from "../../../../shared/services/leave.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {LeaveRequest} from "../../../../shared/models/leave-request.model";
import {JwtHelperService} from "@auth0/angular-jwt";
import {AuthenticationService} from "../../../../auth/service";
import {UserListService} from "../../user/user-list/user-list.service";

@Component({
    selector: 'app-leave-add',
    templateUrl: './leave-add.component.html',
    styleUrls: ['./leave-add.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LeaveAddComponent implements OnInit {
    public contentHeader: object;
    userEmail: string;
    leaveType: string;
    leaveTypeADMIN: string;
    currentUser
    role
    reason: string;
    exceptionalLeaveType: string;
    timeOfDay: string;
    startDate: Date = new Date();
    endDate: Date = new Date();
    public leaveTypes = [
        {name: 'Personal Leave', value: 'PERSONAL_LEAVE'},
        {name: 'Exceptional leave', value: 'EXCEPTIONAL_LEAVE'},
        {name: 'Half Day', value: 'HALF_DAY'},
    ];
    public leaveTypesADMIN = [
        {name: 'Personal Leave', value: 'PERSONAL_LEAVE'},
        {name: 'Exceptional leave', value: 'EXCEPTIONAL_LEAVE'},
        {name: 'Sick Leave', value: 'SICK_LEAVE'},
        {name: 'Half Day', value: 'HALF_DAY'},
    ];

    public timesOfDay = [
        {name: 'Morning', value: 'MORNING'},
        {name: 'Afternoon', value: 'AFTERNOON'},
        {name: 'Inapplicable', value: 'INAPPLICABLE'},
    ];

    public exceptionalLeaveTypes = [
        {name: 'Maternity Leave', value: 'MATERNITY_LEAVE'},
        {name: 'Paternity Leave', value: 'PATERNITY_LEAVE'},
        {name: 'Parent Death Leave', value: 'PARENT_DEATH_LEAVE'},
        {name: 'Marriage Leave', value: 'MARRIAGE_LEAVE'},
        {name: 'Bereavement Leave', value: 'BEREAVEMENT_LEAVE'},
        {name: 'Child Birth Leave', value: 'CHILD_BIRTH_LEAVE'},
        {name: 'Child Marriage Leave', value: 'CHILD_MARRIAGE_LEAVE'},
        {name: 'Child Death Leave', value: 'CHILD_DEATH_LEAVE'},
        {name: 'None', value: 'NONE'}
    ];
    isAdmin: boolean
    isClient: boolean
    selectUser: any[] = [{
        id: 1,
        name: 'Chrome',
        icon: 'fa fa-chrome'
    },
        {
            id: 2,
            name: 'Firefox',
            icon: 'fa fa-firefox'
        },
        {
            id: 3,
            name: 'Safari',
            icon: 'fa fa-safari'
        },
        {
            id: 4,
            name: 'Opera',
            icon: 'fa fa-opera'
        }];

    constructor(private router: Router,
                private toastr: ToastrService,
                private leaveService: LeaveService,
                private userService: UserListService,
                private jwtService: JwtHelperService,
                private _authenticationService: AuthenticationService,) {

        this._authenticationService.currentUser.subscribe(x => (this.currentUser = x));
        this.isAdmin = this._authenticationService.isAdmin;
        this.isClient = this._authenticationService.isClient;
    }

    public getFilteredLeaveTypes() {
        if (this.role === 'ADMIN') {
            return this.leaveTypes;
        } else {
            return this.leaveTypes.filter(leaveType => leaveType.value !== 'SICK_LEAVE');
        }
    }

    ngOnInit(): void {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
        this.role = this.currentUser.role
        this.userEmail = this.currentUser.email
        this.startDate = new Date()
        this.endDate = new Date()
        console.log("user role", this.role)
        this._authenticationService.currentUser.subscribe(x => (this.currentUser = x));
        this.isAdmin = this._authenticationService.isAdmin;
        this.isClient = this._authenticationService.isClient;
        this.getUsers();
        this.contentHeader = {
            headerTitle: 'Create Leave Request',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Leave Requests',
                        isLink: true,
                        link: '/apps/leave/list'
                    },
                    {
                        name: 'Create Leave Request',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };

    }

    addLeave() {
        const leaveRequest = new LeaveRequest();
        leaveRequest.startDate = this.startDate;
        leaveRequest.endDate = this.endDate;
        leaveRequest.leaveType = this.leaveType;

        if (this.role === "ADMIN") {
            leaveRequest.leaveType = this.leaveTypeADMIN;
        } else {
            leaveRequest.leaveType = this.leaveType;
        }

        leaveRequest.reason = this.reason;
        leaveRequest.exceptionalLeaveType = this.exceptionalLeaveType;
        leaveRequest.timeOfDay = this.timeOfDay;
        leaveRequest.userEmail = this.userEmail;
        console.log(leaveRequest)

        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        this.leaveService.addLeaveRequest(currentUserEmail, leaveRequest).subscribe(
            (data) => {
                this.toastr.success('Leave Request added successfully', 'Success', {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    tapToDismiss: true,
                    timeOut: 5000
                });
                this.router.navigate(['/apps/leave/list']);
            },
            (error) => {
                this.toastr.error('Error adding team', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            }
        );
    }

    onExceptionalLeaveTypeChange(selectedItem: any) {
        this.exceptionalLeaveType = selectedItem.value;
    }

    onTimeOfDayChange(selectedItem: any) {
        this.timeOfDay = selectedItem.value;
    }

    onLeaveTypeChange(selectedItem: any) {
        if (this.role === "ADMIN") {
            this.leaveTypeADMIN = selectedItem.value;
        } else {
            this.leaveType = selectedItem.value;
        }
        console.log(this.leaveType + " " + this.leaveTypeADMIN)
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    onUserEmailChange(selectedItem: any) {
        this.userEmail = selectedItem.email;

    }
}
