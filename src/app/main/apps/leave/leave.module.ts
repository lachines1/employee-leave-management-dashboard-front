import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LeaveListComponent} from './leave-list/leave-list.component';
import {LeaveAddComponent} from './leave-add/leave-add.component';
import {LeaveViewComponent} from './leave-view/leave-view.component';
import {CardSnippetModule} from "../../../../@core/components/card-snippet/card-snippet.module";
import {ContentHeaderModule} from "../../../layout/components/content-header/content-header.module";
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {RouterModule} from "@angular/router";
import {CoreDirectivesModule} from "../../../../@core/directives/directives";
import {CsvModule} from "@ctrl/ngx-csv";
import {NgbDropdownModule} from "@ng-bootstrap/ng-bootstrap";
import { LeaveEditComponent } from './leave-edit/leave-edit.component';

const routes = [
    {
        path: 'list',
        component: LeaveListComponent
    },
    {
        path: 'add',
        component: LeaveAddComponent
    },
    {
        path: 'view/:id',
        component: LeaveViewComponent
    },
    {
        path: 'edit/:id',
        component: LeaveEditComponent
    },
]

@NgModule({
    declarations: [
        LeaveListComponent,
        LeaveAddComponent,
        LeaveViewComponent,
        LeaveEditComponent
    ],
    exports: [
        LeaveListComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CardSnippetModule,
        ContentHeaderModule,
        CoreDirectivesModule,
        CsvModule,
        FormsModule,
        NgSelectModule,
        NgxDatatableModule,
        NgbDropdownModule,
    ]
})
export class LeaveModule {
}
