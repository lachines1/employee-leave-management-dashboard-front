import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TeamService} from "../../../../shared/services/team.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {TeamRequest} from "../../../../shared/models/team-request.model";
import {UserListService} from "../../user/user-list/user-list.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "../../../../auth/models";
import {OrganizationalUnitService} from "../../../../shared/services/organizational-unit.service";

@Component({
    selector: 'app-team-edit',
    templateUrl: './team-edit.component.html',
    styleUrls: ['./team-edit.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TeamEditComponent implements OnInit {
    public teamRequest: TeamRequest = {};
    teamName: string;
    teamDescription: string;
    teamMembers: any;
    teamLeadEmail: string;
    organizationalUnitName: string;
    contentHeader: object;
    selectUser: User[];
    departments: any;
    private teamId: number;

    constructor(private teamService: TeamService,
                private router: Router,
                private userService: UserListService,
                private organizationUnitService: OrganizationalUnitService,
                private jwtService: JwtHelperService,
                private toastr: ToastrService,
                private route: ActivatedRoute
    ) {
        this.route.params.subscribe(params => {
            this.teamId = params['id'];
            this.getTeamById(this.teamId);
        });

    }

    ngOnInit(): void {
        this.getUsers();
        this.getDepartments();
        this.contentHeader = {
            headerTitle: 'Edit Team',
            actionButton: true,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Home',
                        isLink: true,
                        link: '/'
                    },
                    {
                        name: 'Teams',
                        isLink: true,
                        link: '/apps/teams/list'
                    },
                    {
                        name: 'Edit Team',
                        isLink: false,
                        link: '/'
                    }

                ]
            }
        };
    }


    editTeam() {
        this.route.params.subscribe(params => {
            this.teamId = params['id'];
            this.getTeamById(this.teamId);
        });
        this.teamRequest.name = this.teamName;
        this.teamRequest.description = this.teamDescription;
        this.teamRequest.teamLeadEmail = this.teamLeadEmail;
        this.teamRequest.organizationalUnitName = this.organizationalUnitName;
        console.log('hello ' + this.teamRequest);
        this.teamService.updateTeam(this.teamId, this.teamRequest).subscribe(
            data => {
                this.toastr.success('Team updated successfully', 'Success', {
                    progressBar: true,
                    progressAnimation: 'increasing',
                    closeButton: true,
                    positionClass: 'toast-top-right',
                    toastClass: 'toast ngx-toastr toast-success',
                    titleClass: 'toast-title',
                    messageClass: 'toast-message',
                    tapToDismiss: true,
                    timeOut: 5000

                });
                this.router.navigate(['/apps/teams/list']);
            },
            error => {
                this.toastr.error('Team update failed', 'Error', {
                    timeOut: 3000,
                    positionClass: 'toast-top-right',
                    closeButton: true,
                    progressBar: true,
                    progressAnimation: 'increasing',
                    toastClass: 'ngx-toastr toast',
                });
            }
        );
    }

    getUsers() {
        const token = localStorage.getItem('token');
        const currentUserEmail = this.jwtService.decodeToken(token).email;
        const role = this.jwtService.decodeToken(token).role;
        if (role === 'ADMIN') {
            this.userService.getAllUsers().subscribe(
                (data) => {
                    this.selectUser = data.filter(user => user.role === 'MANAGER');
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else if (role === 'MANAGER') {
            this.userService.getUsersByManager(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = data;
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        } else {
            this.userService.getUserByEmail(currentUserEmail).subscribe(
                (data) => {
                    this.selectUser = [data];
                },
                (error) => {
                    this.toastr.error('Error fetching users', 'Error', {
                        timeOut: 3000,
                        closeButton: true,
                        progressBar: true,
                        toastClass: 'toast ngx'
                    });
                });
        }
    }

    getDepartments() {
        this.organizationUnitService.getAllOrganizationalUnits().subscribe(
            (data) => {
                this.departments = data;
            },
            (error) => {
                this.toastr.error('Error fetching departments', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            });
    }

    onUserEmailChange(selectedItem: any) {
        this.teamLeadEmail = selectedItem.email;
    }

    onDepartmentChange(selectedItem: any) {
        this.organizationalUnitName = selectedItem.name;
    }

    private getTeamById(teamId: number) {
        this.teamService.getTeamById(teamId).subscribe(
            (data) => {
                this.teamRequest = data;
                this.teamName = this.teamRequest.name;
                this.teamDescription = this.teamRequest.description;
                this.teamLeadEmail = this.teamRequest.teamLeadEmail;
                this.organizationalUnitName = this.teamRequest.organizationalUnitName;
            },
            (error) => {
                this.toastr.error('Error fetching team', 'Error', {
                    timeOut: 3000,
                    closeButton: true,
                    progressBar: true,
                    toastClass: 'toast ngx-toastr',
                });
            });

    }
}
