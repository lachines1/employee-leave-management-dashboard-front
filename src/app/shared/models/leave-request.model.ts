export class LeaveRequest {

    userEmail?: string;
    leaveType?: string;
    exceptionalLeaveType?: string;
    timeOfDay?: string;
    startDate?: Date;
    endDate?: Date;
    reason?: string;
}
