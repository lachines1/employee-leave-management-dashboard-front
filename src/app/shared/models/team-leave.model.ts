import {Team} from "./team.model";

export interface TeamLeave {
    id: number
    startDate: Date
    endDate: Date
    reason: string
    status: string
    createdAt: string
    team: Team
}

