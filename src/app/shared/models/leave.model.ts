import {User} from "./user.model";

export interface Leave {
    id: number
    leaveType: string
    reason: string
    exceptionalLeaveType: string
    timeOfDay: any
    startDate: string
    endDate: string
    createdAt: string
    status: string
    user: User
}

