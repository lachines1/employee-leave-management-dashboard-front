export class TeamRequest {
    name?: string;
    description?: string;
    teamLeadEmail?: string;
    organizationalUnitName?: string;
}
