import {User} from "./user.model";

export interface Team {
    id: number
    name: string
    description: string
    createdAt: string
    manager: User
    members: User[]
    organizationalUnit: any
    minimumAttendance: number
}



