import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {LeaveRequest} from "../models/leave-request.model";

@Injectable({
    providedIn: 'root'
})
export class LeaveService {
    apiUrl = `http://localhost:8080`

    constructor(private http: HttpClient) {
    }

    addLeaveRequest(currentUserEmail: string, leaveRequest: LeaveRequest): Observable<void> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };

        const params = new HttpParams()
            .set('currentUserEmail', currentUserEmail);

        return this.http.post<void>(`${this.apiUrl}/api/v1/employee-leave/user/create`, leaveRequest, {
            headers: httpOptions.headers,
            params
        });
    }

    treatLeaveRequest(currentUserEmail: string, leaveRequestId: number, status: string): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };

        const params = new HttpParams()
            .set('currentUserEmail', currentUserEmail)
            .set('leaveRequestId', leaveRequestId.toString())
            .set('status', status);

        return this.http.post<any>(`${this.apiUrl}/api/v1/employee-leave/manager/treat`, {}, {
            headers: httpOptions.headers,
            params
        });
    }

    updateLeaveRequest(leaveRequestId: number, leaveRequest: LeaveRequest): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };

        return this.http.put<any>(`${this.apiUrl}/api/v1/employee-leave/update/${leaveRequestId}`, leaveRequest, httpOptions);
    }

    deleteLeaveRequest(leaveRequestId: number): Observable<void> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };

        return this.http.delete<void>(`${this.apiUrl}/api/v1/employee-leave/delete/${leaveRequestId}`, httpOptions);
    }

    getLeaveRequestById(leaveRequestId: number): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/employee-leave/get/${leaveRequestId}`, httpOptions);
    }

    getLeaveRequestsByUserEmail(userEmail: string): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/employee-leave/user/${userEmail}`, httpOptions);
    }

    getAllLeaveRequests(): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/employee-leave/all`, httpOptions);
    }

    getLeaveRequestsByUserId(userId: number): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/employee-leave/user/${userId}`, httpOptions);
    }

    getLeaveRequestsByManager(managerEmail: string): Observable<any> {
        const token = localStorage.getItem('token');

        const options = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        };

        return this.http.get<any>(`${this.apiUrl}/api/v1/employee-leave/manager/${managerEmail}`, options)
            .pipe(
                catchError((error) => {
                    // Handle errors here, you can log them or perform other actions
                    console.error('Error occurred:', error);
                    return throwError(error);
                })
            );
    }

    getLeaveRequestsByUser(currentUserEmail: any) {
        const token = localStorage.getItem('token');
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/employee-leave/user/${currentUserEmail}`, options)
    }
}
