import {Injectable} from '@angular/core';
import {User} from "../../auth/models";
import {JwtHelperService} from "@auth0/angular-jwt";


@Injectable({
    providedIn: 'root'
})
export class JwtUtilService {

    private user: User; // You can define the user object with your required fields.

    constructor(private jwtHelper: JwtHelperService) {
    }

    // This method is called when you receive the JWT token from the backend.
    public setToken(token: string) {
        // Decode the JWT token to access the payload.
        const tokenPayload = this.jwtHelper.decodeToken(token);

        // Create the user object with the required details.
        this.user = {
            email: tokenPayload.email,
            id: tokenPayload.id,
            lastName: tokenPayload.lastName,
            firstName: tokenPayload.firstName,
            role: tokenPayload.role,
            avatar: "assets/images/avatars/1.png"
            // Add other user details as needed.
        };
    }

    // This method allows you to access the user object.
    public getUser() {
        return this.user;
    }
}
