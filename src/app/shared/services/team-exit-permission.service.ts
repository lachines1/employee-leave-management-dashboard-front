import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {TeamExitPermissionRequest} from "../models/team-exit-permission-request.model";

@Injectable({
    providedIn: 'root'
})
export class TeamExitPermissionService {

    private apiUrl = 'http://localhost:8080';

    constructor(private http: HttpClient) {
    }

    createTeamExitPermission(currentUserEmail: string, request: TeamExitPermissionRequest): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        const params = new HttpParams()
            .set('currentUserEmail', currentUserEmail);

        return this.http.post<any>(`${this.apiUrl}/api/v1/team-exit-permission/create`, request, {
            headers: httpOptions.headers,
            params
        });
    }

    getAllTeamExitPermissions(): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/team-exit-permission/all`, httpOptions);
    }

    getTeamExitPermissionById(id: number): Observable<any> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/team-exit-permission/get/${id}`, httpOptions);
    }

    deleteTeamExitPermissionById(id: number): Observable<void> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            })
        };

        return this.http.delete<void>(`${this.apiUrl}/api/v1/team-exit-permission/delete/${id}`, httpOptions);
    }

    treatTeamExitPermission(id: number, status: string, currentUserEmail: string): Observable<void> {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`
            })
        };

        const params = new HttpParams()
            .set('status', status)
            .set('currentUserEmail', currentUserEmail);

        return this.http.put<void>(`${this.apiUrl}/api/v1/team-exit-permission/treat/${id}`, {}, {
            headers: httpOptions.headers,
            params
        });
    }

    getTeamExitPermissionsByTeam(teamName: any) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/team-exit-permission/team/get-by-team/${teamName}`, httpOptions);
    }

    getTeamExitPermissionsByManager(currentUserEmail: any) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/team-exit-permission/manager/get-by-manager/${currentUserEmail}`, httpOptions);
    }

    editExternalAuthorization(teamExitPermissionId: number, teamExitPermissionRequest: TeamExitPermissionRequest) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // Add the token as a Bearer token
            })
        };

        return this.http.put<any>(`${this.apiUrl}/api/v1/team-exit-permission/update/${teamExitPermissionId}`, teamExitPermissionRequest, httpOptions);
    }

    getTeamExitPermissionsByUser(currentUserEmail: any) {
        const token = localStorage.getItem('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer ${token}`
            })
        };
        return this.http.get<any>(`${this.apiUrl}/api/v1/team-exit-permission/user/get-by-user/${currentUserEmail}`, httpOptions);


    }
}
